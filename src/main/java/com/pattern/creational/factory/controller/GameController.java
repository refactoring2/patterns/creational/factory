package com.pattern.creational.factory.controller;

import com.pattern.creational.factory.game.Game;
import com.pattern.creational.factory.service.GameService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GameController {

    private final GameService gameSettingsService;

    @GetMapping("/{type}/games")
    public Game getGame(@PathVariable String type) {
        return gameSettingsService.getGameByDifficulty(type);
    }

    public GameController(GameService gameSettingsService) {
        this.gameSettingsService = gameSettingsService;
    }
}
