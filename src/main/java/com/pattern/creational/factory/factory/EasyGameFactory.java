package com.pattern.creational.factory.factory;

import com.pattern.creational.factory.game.EasyGame;
import com.pattern.creational.factory.game.Game;
import org.springframework.stereotype.Service;

@Service("easy")
public class EasyGameFactory implements GameFactory{

    @Override
    public Game createNewGame() {
        return new EasyGame();
    }
}
