package com.pattern.creational.factory.factory;

import com.pattern.creational.factory.game.Game;

public interface GameFactory {

    Game createNewGame();

}
