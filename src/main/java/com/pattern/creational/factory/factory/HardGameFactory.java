package com.pattern.creational.factory.factory;

import com.pattern.creational.factory.game.Game;
import com.pattern.creational.factory.game.HardGame;
import org.springframework.stereotype.Service;

@Service("hard")
public class HardGameFactory implements GameFactory{

    @Override
    public Game createNewGame() {
        return new HardGame();
    }
}
