package com.pattern.creational.factory.factory;

import com.pattern.creational.factory.game.Game;
import com.pattern.creational.factory.game.MiddleGame;
import org.springframework.stereotype.Service;

@Service("middle")
public class MiddleGameFactory implements GameFactory{
    @Override
    public Game createNewGame() {
        return new MiddleGame();
    }
}
