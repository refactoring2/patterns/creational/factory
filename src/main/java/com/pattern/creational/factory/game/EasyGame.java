package com.pattern.creational.factory.game;

import com.pattern.creational.factory.model.Enemy;

import java.util.Set;

public class EasyGame implements Game {

    @Override
    public String startGame() {
        return "Easy game was started!";
    }

    @Override
    public Set<Enemy> getEnemies() {
        return Set.of(new Enemy("Poncho", 1));
    }
}
