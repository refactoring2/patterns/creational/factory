package com.pattern.creational.factory.game;

import com.pattern.creational.factory.model.Enemy;

import java.util.Set;

public interface Game {

    String startGame();
    Set<Enemy> getEnemies();
}
