package com.pattern.creational.factory.game;

import com.pattern.creational.factory.model.Enemy;

import java.util.Set;

public class MiddleGame implements Game{

    @Override
    public String startGame() {
        return "Middle game was started!";
    }

    @Override
    public Set<Enemy> getEnemies() {
        return Set.of(
                new Enemy("Poncho", 1),
                new Enemy("Blaine", 4));
    }
}
