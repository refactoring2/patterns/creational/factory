package com.pattern.creational.factory.model;

import java.util.Optional;

import static java.util.Optional.ofNullable;

public class Enemy {

    private final String name;
    private final int strength;

    public Optional<String> getName() {
        return ofNullable(name);
    }

    public int getStrength(){
        return strength;
    }

    public Enemy(String name, int strength) {
        this.name = name;
        this.strength = strength;
    }
}
