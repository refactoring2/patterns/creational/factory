package com.pattern.creational.factory.service;

import com.pattern.creational.factory.factory.GameFactory;
import com.pattern.creational.factory.game.Game;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class GameService {

    private final GameFactory easyGameFactory;
    private final GameFactory middleGameFactory;
    private final GameFactory hardGameFactory;

    public Game getGameByDifficulty(String type){
        return getGameFactoryBy(type).createNewGame();
    }

    private GameFactory getGameFactoryBy(String type){
        if("easy".equals(type)) return easyGameFactory;
        if("middle".equals(type)) return middleGameFactory;
        if("hard".equals(type)) return hardGameFactory;

        throw  new UnsupportedOperationException();
    }

    public GameService(@Qualifier("easy") GameFactory easyGameFactory, @Qualifier("middle") GameFactory middleGameFactory, @Qualifier("hard") GameFactory hardGameFactory) {
        this.easyGameFactory = easyGameFactory;
        this.middleGameFactory = middleGameFactory;
        this.hardGameFactory = hardGameFactory;
    }
}
