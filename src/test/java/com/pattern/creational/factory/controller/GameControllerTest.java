package com.pattern.creational.factory.controller;

import com.pattern.creational.factory.game.EasyGame;
import com.pattern.creational.factory.game.HardGame;
import com.pattern.creational.factory.game.MiddleGame;
import com.pattern.creational.factory.service.GameService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(GameController.class)
public class GameControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GameService gameService;

    @Test
    void shouldSetupEasyGameDifficulty() throws Exception {
        when(gameService.getGameByDifficulty(anyString())).thenReturn(new EasyGame());

        mockMvc.perform(MockMvcRequestBuilders.get("/{type}/games", "easy"))
                .andExpect(status().isOk());

        verify(gameService, times(1)).getGameByDifficulty(anyString());
    }

    @Test
    void shouldSetupMiddleGameDifficulty() throws Exception {
        when(gameService.getGameByDifficulty(anyString())).thenReturn(new MiddleGame());

        mockMvc.perform(MockMvcRequestBuilders.get("/{type}/games", "middle"))
                .andExpect(status().isOk());

        verify(gameService, times(1)).getGameByDifficulty(anyString());
    }

    @Test
    void shouldSetupHardGameDifficulty() throws Exception {
        when(gameService.getGameByDifficulty(anyString())).thenReturn(new HardGame());

        mockMvc.perform(MockMvcRequestBuilders.get("/{type}/games", "hard"))
                .andExpect(status().isOk());
        verify(gameService, times(1)).getGameByDifficulty(anyString());
    }
}
