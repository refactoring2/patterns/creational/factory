package com.pattern.creational.factory.factory;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class EasyGameFactoryTest {

    @InjectMocks
    private EasyGameFactory easyGameFactory;

    @Test
    void shouldReturnEasyGame(){
        var actual = easyGameFactory.createNewGame();
        assertEquals(1, actual.getEnemies().size());
    }
}
