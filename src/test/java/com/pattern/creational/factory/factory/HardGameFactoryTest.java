package com.pattern.creational.factory.factory;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class HardGameFactoryTest {

    @InjectMocks
    private HardGameFactory hardGameFactory;

    @Test
    void shouldReturnHardGame(){
        var actual = hardGameFactory.createNewGame();
        assertEquals(3, actual.getEnemies().size());
    }
}
