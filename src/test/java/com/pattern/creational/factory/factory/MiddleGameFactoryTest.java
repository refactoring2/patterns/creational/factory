package com.pattern.creational.factory.factory;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class MiddleGameFactoryTest {

    @InjectMocks
    private MiddleGameFactory middleGameFactory;

    @Test
    void shouldReturnHardGame(){
        var actual = middleGameFactory.createNewGame();
        assertEquals(2, actual.getEnemies().size());
    }
}
