package com.pattern.creational.factory.game;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EasyGameTest {

    private final Game easyGame = new EasyGame();

    @Test
    void shouldReturnEasySetupOfEnemies() {
        var actual = easyGame.getEnemies();
        assertEquals(1, actual.size());
    }

    @Test
    void shouldReturnEasyGame(){
        var actual = easyGame.startGame();
        assertEquals("Easy game was started!", actual);
    }
}
