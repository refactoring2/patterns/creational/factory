package com.pattern.creational.factory.game;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HardGameTest {

    private final Game hardGame = new HardGame();

    @Test
    void shouldReturnHardGame(){
        var actual = hardGame.startGame();
        assertEquals("Hard game was started!", actual);
    }

    @Test
    void shouldReturnHardSetupOfEnemies(){
        var actual = hardGame.getEnemies();
        assertEquals(3, actual.size());
    }
}
