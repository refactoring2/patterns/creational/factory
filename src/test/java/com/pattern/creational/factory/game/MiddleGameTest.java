package com.pattern.creational.factory.game;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MiddleGameTest {

    private final Game middleGame = new MiddleGame();

    @Test
    void shouldReturnMiddleSetupOfEnemies(){
        var actual = middleGame.getEnemies();
        assertEquals(2, actual.size());
    }

    @Test
    void shouldReturnMiddleGame(){
        var actual = middleGame.startGame();
        assertEquals("Middle game was started!", actual);
    }
}
