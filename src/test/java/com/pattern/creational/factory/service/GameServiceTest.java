package com.pattern.creational.factory.service;

import com.pattern.creational.factory.factory.GameFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class GameServiceTest {

    private @Autowired @Qualifier("easy") GameFactory easyGameFactory;
    private @Autowired @Qualifier("middle") GameFactory middleGameFactory;
    private @Autowired @Qualifier("hard") GameFactory hardGameFactory;

    private GameService gameService;

    @BeforeEach
    void init() {
        gameService = new GameService(easyGameFactory, middleGameFactory, hardGameFactory);
    }

    @Test
    void shouldReturnEasyGame() {
        var type = "easy";

        var actual = gameService.getGameByDifficulty(type);

        assertEquals(1, actual.getEnemies().size());

    }

    @Test
    void shouldReturnMiddleGame(){
        var type = "middle";

        var actual = gameService.getGameByDifficulty(type);

        assertEquals(2, actual.getEnemies().size());
    }

    @Test
    void shouldReturnHardGame(){
        var type = "hard";

        var actual = gameService.getGameByDifficulty(type);

        assertEquals(3, actual.getEnemies().size());
    }
}
